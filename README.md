# CMCSRV

## CREATE BASIC SERVER
The command creates a simple Spigot Server.

`bash cmcsrv.sh --dl spigot --c`

## CREATE SPIGOT SERVER WITH EXTRAS
The command creates a Spigot server with the version 1.20.4, with the servername "My Server" and Motd description "My best server". The server has a active whitelist, our username is active owner and the server becomes 8G ram.

`bash cmcsrv.sh --dl spigot --v "1.20.4" --sn "My Server" --m "My best server" --oi "MyIngameName123" --w true --r 8 G --c`

## COMMANDS
<details><summary>Display this help and exit</summary>

`bash cmcsrv.sh -h`
</details>
<details><summary>Show version of this script</summary>

`bash cmcsrv.sh -v`
</details>

### 
<details><summary>Create Minecraft server</summary>

`bash cmcsrv.sh`

***with star marked is optional**

*DOWNLOAD SPIGOT OR CRAFTBUKKIT `--dl spigot|craftbukkit`

*VERSION `--v "1.20.1"`

*SERVERNAME `--sn "Servername"`

*MOTD TEXT `--m "Motd text"`

*LEVELNAME `--ln "world"`

CONNECTION IP `--ci "mc.example.com"`

*OWNER INGAME NAME `--oi "DeanLP"`

*MAX PLAYERS `--mp 35`

*WHITELIST `--w true`

*RCON PASSWORD `--rpwd "passwd1234"`

*RCON PORT `--rp 25575`

*SERVER PORT `--p 25565`

*RAM `--r 4096 M|4 G`

*AUTO BACKUP `--ab 7 d|s|r|sr|ds|dr|dsr`

CREATE SERVER `--c`
</details>

<details><summary>Recreate Minecraft world</summary>

`bash cmcsrv.sh --rcw`
</details>

<details><summary>Upgrade Minecraft server</summary>

`bash cmcsrv.sh --upgrade "1.20.x"`
</details>

<details><summary>Install plugins</summary>

`bash cmcsrv.sh --iplg "https://www.spigotmc.org/resources/report.54161/download?version=222426"`
</details>

<details><summary>Create Backup</summary>

`bash cmcsrv.sh --cb`
</details>

<details><summary>Remove Minecraft server</summary>

`bash cmcsrv.sh --remove`
</details>

### 
<details><summary>Start Minecraft server</summary>

`bash cmcsrv.sh --start`
</details>

<details><summary>Get Status from Minecraft server</summary>

`bash cmcsrv.sh --status`
</details>

<details><summary>Get Live-Log from Minecraft server</summary>

`bash cmcsrv.sh --live-log`
</details>

<details><summary>Send messages on Minecraft server</summary>

`bash cmcsrv.sh --say`
</details>

<details><summary>Reload Minecraft server</summary>

`bash cmcsrv.sh --reload`
</details>

<details><summary>Restart Minecraft server</summary>

`bash cmcsrv.sh --restart`
</details>

<details><summary>Stop Minecraft server</summary>

`bash cmcsrv.sh --stop`
</details>

## SUPPORTED AND TESTED VERSIONS

| Java Version | Minecraft Version |
| ------ | ------ |
| <= 15 | 1.12, 1.13, 1.14, 1.15, 1.16 |
| 16 | 1.17 |
| >= 17 | 1.18, 1.19, 1.20 |