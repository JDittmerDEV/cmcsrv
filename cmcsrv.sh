#!/bin/sh
# POSIX

INSTALLPATH="/opt"
MC_FOLDERNAME="minecraft$RANDOM"

# colors
COLOR_RED='\033[0;31m'
COLOR_YELLOW='\033[0;33m'
COLOR_GREEN='\033[0;32m'
COLOR_NC='\033[0m'

update_upgrade() {
    echo " "
    echo -e "${COLOR_GREEN}Update Server ...${COLOR_NC}"
    apt-get -qq -y update
}

show_logo() {
    # http://patorjk.com/software/taag/#p=display&c=bash&f=Big&t=C%20MC%20SRV%0A%2C......................%2C%0A%20%20%20V%20.%201%20.%200%20.%200
cat << EOF
     _____   __  __  _____    _____ _______      __ 
    / ____| |  \/  |/ ____|  / ____|  __ \ \    / / 
   | |      | \  / | |      | (___ | |__) \ \  / /  
   | |      | |\/| | |       \___ \|  _  / \ \/ /   
   | |____  | |  | | |____   ____) | | \ \  \  /    
    \_____| |_| _|_|\_____| |_____/|_|_ \_\ _\/ _ _ 
   ( |_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| )
   |/ \ \    / /     /_ |      / _ \       / _ \ |/ 
       \ \  / /       | |     | | | |     | | | |   
        \ \/ /        | |     | | | |     | | | |   
         \  /     _   | |  _  | |_| |  _  | |_| |   
          \/     (_)  |_| (_)  \___/  (_)  \___/    
                                                    
                                                    
EOF
}

# Show help
show_help() {
cat << EOF
    ===========================================================================================
    ||                                       EXAMPLES:                                       ||
    ===========================================================================================

    Display this help and exit              bash cmcsrv.sh  -h|-help
    Show version of this script             bash cmcsrv.sh  -v|-version

    Create Minecraft server                 bash cmcsrv.sh
        * with star marked is optional

        - * DOWNLOAD SPIGOT/CRAFTBUKKIT                     --dl spigot|craftbukkit
        - * VERSION                                         --v "1.20.1"
        - * SERVERNAME                                      --sn "Servername"
        - * MOTD TEXT                                       --m "Motd text"
        - * LEVELNAME                                       --ln "world"
        - CONNECTION IP                                     --ci "mc.example.com"
        - * OWNER INGAME NAME                               --oi "DeanLP"
        - * MAX PLAYERS                                     --mp 35
        - * WHITELIST                                       --w true
        - * RCON PASSWORD                                   --rpwd "passwd1234"
        - * RCON PORT                                       --rp 25575
        - * SERVER PORT                                     --p 25565
        - * RAM                                             --r 4096 M|4 G
        - * AUTO BACKUP                                     --ab 7 d|s|r|sr|ds|dr|dsr
        - CREATE SERVER                                     --c
    Recreate Minecraft world                bash cmcsrv.sh --rcw
    Upgrade Minecraft server                bash cmcsrv.sh --upgrade "1.20.x"
    Install Plugins                         bash cmcsrv.sh --iplg "https://www.spigotmc.org/resources/report.54161/download?version=222426"
    Create Backup                           bash cmcsrv.sh --cb
    Remove Minecraft server                 bash cmcsrv.sh --remove

    Start Minecraft server                  bash cmcsrv.sh --start
    Get Status from Minecraft server        bash cmcsrv.sh --status
    Get Live-Log from Minecraft server      bash cmcsrv.sh --live-log
    Send messages on Minecraft server       bash cmcsrv.sh --say
    Reload Minecraft server                 bash cmcsrv.sh --reload
    Restart Minecraft server                bash cmcsrv.sh --restart
    Stop Minecraft server                   bash cmcsrv.sh --stop

EOF
}

# Show version
show_version() {
cat << EOF
                     Author: Justin Dittmer
                    Website: jdittmer.dev   
                     GitLab: https://gitlab.com/JDittmerDEV/cmcsrv

EOF
}

function config {
    echo $(cat "${INSTALLPATH}/$1/_cfg.cmcsrv" | grep -o "^$2:.*" | cut -d':' -f2)
}

function checkJavaVersion {
    current_java_version=$(java --version | grep "java " | cut -d' ' -f2 | cut -d'.' -f1)
            
    if [ $(echo "$1" | cut -d'.' -f2 | grep -o -E '^(12|13|14|15|16)$') ]; then
        if [ $(expr $current_java_version) -lt 16 ]; then
            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
        else
            echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 15 or lower for this Minecraft version!"
            if [ $check_requirements == true ]; then
                check_requirements=false
            fi
        fi
    elif [ $(echo "$1" | cut -d'.' -f2 | grep -o -E '^(17)$') ]; then
        if [ $(expr $current_java_version) -eq 16 ]; then
            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
        else
            echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 16 for this Minecraft version!"
            if [ $check_requirements == true ]; then
                check_requirements=false
            fi
        fi
    elif [ $(echo "$1" | cut -d'.' -f2 | grep -o -E '^(18|19|20)$') ]; then
        if [ $(expr $current_java_version) -gt 16 ]; then
            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Java Version > GOOD!"
        else
            echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Java Version > You need Java 17 or higher for this Minecraft version!"
            if [ $check_requirements == true ]; then
                check_requirements=false
            fi
        fi
    else
        echo -e "   [${COLOR_YELLOW}INFO${COLOR_NC}]: Java Version > This Minecraft version is currently not supported in the script. Please contact the creator for an update!"
        if [ $check_requirements == true ]; then
            check_requirements=false
        fi
    fi
}

list_servers() {
    dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

    c=1
    for i in "${dirs[@]}"
    do 
        servername=$(cat "/opt/$i/_cfg.cmcsrv" | grep -o -E "srvname:.*" | cut -d':' -f2)
        serverip=$(cat "/opt/$i/_cfg.cmcsrv" | grep -o -E "^connip:.*" | cut -d':' -f2)
        serverport=$(cat "/opt/$i/_cfg.cmcsrv" | grep -o -E "^port:.*" | cut -d':' -f2)

        if [ $(systemctl show -p SubState --value ${dirs[$c-1]}) == "running" ]; then
            echo -e "$c) ${COLOR_GREEN}[  ONLINE ]${COLOR_NC} ${servername} - (${serverip}:${serverport})"
            c=$((c+1))
        elif [ $(systemctl show -p SubState --value ${dirs[$c-1]}) == "dead" ]; then
            echo -e "$c) ${COLOR_RED}[ OFFLINE ]${COLOR_NC} ${servername} - (${serverip}:${serverport})"
            c=$((c+1))
        elif [ $(systemctl show -p SubState --value ${dirs[$c-1]}) != "dead" ]; then
            echo -e "$c) ${COLOR_YELLOW}[ PROBLEM !! $(systemctl show -p SubState --value ${dirs[$c-1]}) ]${COLOR_NC} ${servername} - (${serverip}:${serverport})"
            c=$((c+1))
        fi
    done
}

function rcon {
    $INSTALLPATH/$1/_tools/mcrcon/mcrcon -H $(config $1 "connip") -P $(config $1 "rcon_port") -p $(config $1 "rcon_passwd") "$2" > /dev/null
}

MC_DOWNLOAD="craftbukkit"
MC_VERSION="1.20.4"
MC_SRVNAME="Minecraft Server"
MC_MOTD="Created with CMCSRV by JDittmer.dev"
MC_LVLNAME="world"
MC_CONNIP="127.0.0.1"
MC_OWNERIG=""
MC_MAX_PLAYERS=20
MC_WHITELIST=false
MC_ENRCON=true
MC_RCON_PASSWD="admin12345"
MC_RCON_PORT=$(shuf -i 16000-16999 -n1)
MC_PORT=$(shuf -i 16000-16999 -n1)
MC_RAM=4096
AUTO_BACKUP=false
AUTO_BACKUP_DAYS=
AUTO_BACKUP_ROUTINE=""

while :; do
    case $1 in
        -h|-help)     # Help
            reset
            show_logo
            show_help
            exit
            ;;
        -v|-version)  # Show last version
            reset
            show_logo
            show_version
            exit
            ;;
        --dl)
            if [ ! $(echo $2 | grep -o -E '^(spigot|craftbukkit)$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Only Spigot and Craftbukkit are supported at the moment!"
                exit
            else
                MC_DOWNLOAD=$2
                shift
            fi
            ;;
        --v|--version)
            if [ ! $(echo $2 | grep -o -E '^((([0-9]{1,}).([0-9]{1,}))|(([0-9]{1,}).([0-9]{1,}).([0-9]{1,})))$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The version number is invalid!"
                exit
            else               
                if [ $(expr $2 | cut -d'.' -f2) -gt 11 ] && [ $(expr $2 | cut -d'.' -f2) -lt 21 ]; then
                    if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$2.jar"; then
                        MC_VERSION=$2
                        shift
                    elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$2.jar"; then
                        MC_VERSION=$2
                        shift
                    else
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The requested URL does not exist."
                        echo "         You tried to install \"$MC_DOWNLOAD $2\". Is that correct?"
                        exit
                    fi
                else
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Only Minecraft version 1.12 to 1.20.x is supported!"
                    exit
                fi
            fi
            ;;
        --sn|--srvname)
            MC_SRVNAME=$2
            shift
            ;;
        --m|--motd)
            MC_MOTD=$2
            shift
            ;;
        --ln|--lvlname)
            MC_LVLNAME=$2
            shift
            ;;
        --ci|--connip)
            MC_CONNIP=$2
            shift
            ;;
        --oi|--ownerig)
            MC_OWNERIG=$2
            shift
            ;;
        --mp|--max-players)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{1,}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The maximum number of players must only consist of numbers!"
                exit
            else
                MC_MAX_PLAYERS=$2
                shift
            fi
            ;;
        --w|--whitelist)
            if [ ! $(echo $2 | grep -o -E '^(true|false)$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The whitelist can only be true or false!"
                exit
            else
                MC_WHITELIST=$2
                shift
            fi
            ;;
        --rpwd|--rcon-passwd)
            MC_RCON_PASSWD=$2
            
            ;;
        --rp|--rcon-port)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{4,5}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RCON port must be 4 or 5 digits and consist of numbers!"
                exit
            else
                MC_RCON_PORT=$2
                shift
            fi
            ;;
        --p|--port)
            if [ ! $(expr $2 | grep -o -E '^[0-9]{4,5}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The port must be 4 or 5 digits and consist of numbers!"
                exit
            else
                MC_PORT=$2
                shift
            fi
            ;;
        --r|--ram)
            if [ $(echo $3 | grep -o -E '(M|G)') ]; then
                if [ $(echo $3 | grep -o -E '(M)') ]; then
                    if [ ! $(expr $2 | grep -o -E '^[0-9]{4,}$') ]; then
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The amount of RAM must only consist of numbers!"
                        exit
                    else
                        if [ $2 -lt 1024 ]; then
                            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RAM must be at least 1024 M!"
                            exit
                        else
                            MC_RAM=$2
                            shift
                        fi
                    fi
                    
                elif [ $(echo $3 | grep -o -E '(G)') ]; then
                    if [ ! $(expr $2 | grep -o -E '^[0-9]{1,}$') ]; then
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The amount of RAM must only consist of numbers!"
                        exit
                    else
                        if [ $2 -lt 1 ]; then
                            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The RAM must be at least 1 G!"
                            exit
                        else
                            MC_RAM=$(expr $2 \* 1024)
                            shift
                        fi
                    fi
                fi
            else
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You must specify a size unit (M or G)!"
                exit
            fi
            shift
            ;;
        --ab|--auto-backup)
            AUTO_BACKUP=true

            if [ ! $(expr $2 | grep -o -E '^[0-9]{1,3}$') ]; then
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The days may only consist of numbers and a maximum of three digits!"
                exit
            else
                AUTO_BACKUP_DAYS=$2

                if [ ! $(echo $3 | grep -o -E '^(d|s|r|sr|ds|dr|dsr)$') ]; then
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The backup routine can only be d|s|r|sr|ds|dr|dsr !"
                    exit
                else
                    AUTO_BACKUP_ROUTINE=$3
                    shift
                fi
            fi
            shift
            ;;
        --c|--create)
            reset
            show_logo

            update_upgrade

            SRVPATH="${INSTALLPATH}/${MC_FOLDERNAME}"
            MCUSER=${MC_FOLDERNAME}

            echo " "
            echo -e "${COLOR_GREEN}Checking system requirements ...${COLOR_NC}"
            check_requirements=true
            # CHECK JAVA VERSION
            checkJavaVersion ${MC_VERSION}

            # CHECK AVAILABLE RAM
            free -m | grep -o -E 'Mem: .*|Speicher: .*' > mem.cmcsrv.tmp
            sed -i 's/ \{1,\}/;/g' mem.cmcsrv.tmp
            available_ram=$(cat mem.cmcsrv.tmp | cut -d';' -f7)
            available_ram_for_srv=$(expr $available_ram / 2)

            if [ $(expr $available_ram_for_srv - $MC_RAM) -lt 0 ]; then
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: Memory > The Minecraft server requires ${MC_RAM}M RAM. Your server only has ${available_ram}M RAM left."
                echo "           We recommend choosing a maximum of $(($((${available_ram_for_srv} / 1024)) * 1024))M of RAM."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
            else
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:       Memory > GOOD!"
            fi
            rm mem.cmcsrv.tmp

            # CHECK FUNCTION WGET
            if command -v wget &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:         wget > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: wget > The \"wget\" function is not installed on the server! Install wget ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y wget
            fi

            # CHECK FUNCTION GIT
            if command -v git &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          git > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: git > The \"git\" function is not installed on the server! Install git ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y git
            fi

            # CHECK FUNCTION TAR
            if command -v tar &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          tar > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: tar > The \"tar\" function is not installed on the server! Install tar ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y tar
            fi

            # CHECK FUNCTION MAKE
            if command -v make &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:         make > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: make > The \"make\" function is not installed on the server! Install make ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y make
            fi

            # CHECK FUNCTION SHUF
            if command -v shuf &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:         shuf > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: shuf > The \"shuf\" function is not installed on the server! Install shuf ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y shuf
            fi

            # CHECK FUNCTION LSOF
            if command -v lsof &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:         lsof > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: lsof > The \"lsof\" function is not installed on the server! Install lsof ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y lsof
            fi

            # CHECK FUNCTION JQ
            if command -v jq &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:           jq > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: jq > The \"jq\" function is not installed on the server! Install jq ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y jq
            fi

            # CHECK FUNCTION TOP
            if command -v top &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          top > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: top > The \"top\" function is not installed on the server! Install top ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y top
            fi

            # CHECK FUNCTION SED
            if command -v sed &> /dev/null; then
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]:          sed > GOOD!"
            else
                echo -e "  [${COLOR_RED}ERROR${COLOR_NC}]: sed > The \"sed\" function is not installed on the server! Install sed ..."
                if [ $check_requirements == true ]; then
                    check_requirements=false
                fi
                apt-get install -y sed
            fi
            
            if [ $check_requirements == false ]; then
                exit
            fi

            if [ ! -d ${SRVPATH} ]; then
                echo " "
                echo -e "${COLOR_GREEN}Create Minecraft and tools folder ...${COLOR_NC}"
                mkdir ${SRVPATH}
                mkdir "${SRVPATH}/_tools"
            else
                if [ ! -d "${SRVPATH}/_tools" ]; then
                    echo " "
                    echo -e "${COLOR_GREEN}Create tools folder ...${COLOR_NC}"
                    mkdir "${SRVPATH}/_tools"
                fi
            fi

            echo " "
            echo -e "${COLOR_GREEN}Create CMCSRV config file ...${COLOR_NC}"
            cat << EOF > ${SRVPATH}/_cfg.cmcsrv
######################################
###                                ###
###   DO NOT MODIFY THIS FILE!!!   ###
###                                ###
######################################

dl:$MC_DOWNLOAD
version:$MC_VERSION
srvname:$MC_SRVNAME
motd:$MC_MOTD
lvlname:$MC_LVLNAME
connip:$MC_CONNIP
ownerig:$MC_OWNERIG
max_players:$MC_MAX_PLAYERS
whitelist:$MC_WHITELIST
rcon_passwd:$MC_RCON_PASSWD
rcon_port:$MC_RCON_PORT
port:$MC_PORT
ram:$MC_RAM
auto_backup:$AUTO_BACKUP
auto_backup_days:$AUTO_BACKUP_DAYS
auto_backup_routine:$AUTO_BACKUP_ROUTINE
EOF
            
            if [ $AUTO_BACKUP == true ]; then
                echo " "
                echo -e "${COLOR_GREEN}Create backup routine ...${COLOR_NC}"
                if [ ! -d "${SRVPATH}/_backups" ]; then
                    mkdir "${SRVPATH}/_backups"
                fi

                cat << EOF > ${SRVPATH}/_backup.sh
######################################
###                                ###
###   DO NOT MODIFY THIS FILE!!!   ###
###                                ###
######################################

#!/bin/bash
SRVPATH="${INSTALLPATH}/${MCUSER}"

DAYS=$AUTO_BACKUP_DAYS
ROUTINE=$AUTO_BACKUP_ROUTINE

find \${SRVPATH}/_backups/ -type f -mtime +\$DAYS -name '*.gz' -delete

tar --exclude='_tools' --exclude='_backups' -czf \${SRVPATH}/_backups/server_planned-\$(date +%F-%H-%M).tar.gz \${SRVPATH}
EOF

                if [ $(echo $AUTO_BACKUP_ROUTINE | grep -o -E '(d)') ]; then
                    (crontab -u $(whoami) -l; echo "0 0 * * * ${SRVPATH}/_backup.sh" ) | crontab -u $(whoami) -
                fi
            fi
            
            if ! getent passwd "$MCUSER" > /dev/null 2>&1; then
                echo " "
                echo -e "${COLOR_GREEN}Create Minecraft user ...${COLOR_NC}"
                adduser $MCUSER --gecos "" --disabled-password --disabled-login > /dev/null
            fi

            echo " "
            echo -e "${COLOR_GREEN}Download and install mcrcon ...${COLOR_NC}"
            cd "${SRVPATH}/_tools" && git clone --quiet https://github.com/Tiiffi/mcrcon.git && cd mcrcon && make > /dev/null && make install > /dev/null && cd /opt
            
            echo " "
            echo -e "${COLOR_GREEN}Download $MC_DOWNLOAD $MC_VERSION ...${COLOR_NC}"
            if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                wget -q -O "${SRVPATH}/server.jar" "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
            elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                wget -q -O "${SRVPATH}/server.jar" "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
            fi

            echo " "
            echo -e "${COLOR_GREEN}Accept Minecraft EULA ...${COLOR_NC}"
            echo "eula=true" > "${SRVPATH}/eula.txt"

            echo " "
            echo -e "${COLOR_GREEN}Grant user and folder rights...${COLOR_NC}"
            chown -cR ${MCUSER}:${MCUSER} ${SRVPATH} > /dev/null

            echo " "
            echo -e "${COLOR_GREEN}Create ${MCUSER}.service ...${COLOR_NC}"
            cat << EOF > /lib/systemd/system/${MCUSER}.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=${SRVPATH}/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

            echo " "
            echo -e "${COLOR_GREEN}Enable and start ${MCUSER}.service ...${COLOR_NC}"
            systemctl enable ${MCUSER}.service
            systemctl start ${MCUSER}.service

            while true; do
                if [ $(systemctl show -p SubState --value ${MCUSER}) == "running" ]; then
                    sleep 5
                    break
                fi

                if [ $(systemctl show -p SubState --value ${MCUSER}) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while creating the server!"
                    echo
                    journalctl -p err -b -u ${MCUSER}
                    exit
                fi
            done

            echo " "
            echo -e "${COLOR_GREEN}Configure server.properties ...${COLOR_NC}"
            sed -i "s/level-name=world/level-name=$MC_LVLNAME/gI" "${SRVPATH}/server.properties"
            sed -i "s/motd=A Minecraft Server/motd=$MC_MOTD/gI" "${SRVPATH}/server.properties"
            sed -i "s/max-players=20/max-players=$MC_MAX_PLAYERS/gI" "${SRVPATH}/server.properties"
            sed -i "s/server-ip=/server-ip=$MC_CONNIP/gI" "${SRVPATH}/server.properties"
            sed -i "s/white-list=false/white-list=$MC_WHITELIST/gI" "${SRVPATH}/server.properties"
            sed -i "s/enforce-whitelist=false/enforce-whitelist=$MC_WHITELIST/gI" "${SRVPATH}/server.properties"
            sed -i "s/server-port=25565/server-port=$MC_PORT/gI" "${SRVPATH}/server.properties"
            sed -i "s/enable-rcon=false/enable-rcon=$MC_ENRCON/gI" "${SRVPATH}/server.properties"
            sed -i "s/rcon.port=25575/rcon.port=$MC_RCON_PORT/gI" "${SRVPATH}/server.properties"
            sed -i "s/rcon.password=/rcon.password=$MC_RCON_PASSWD/gI" "${SRVPATH}/server.properties"

            echo " "
            echo -e "${COLOR_GREEN}Restarting Minecraft server ...${COLOR_NC}"
            systemctl stop ${MCUSER}.service
            pgrep -u $MCUSER | xargs kill -9
            sleep 5
            systemctl start ${MCUSER}.service

            if [ ! -z "${MC_OWNERIG}" ]; then
                echo " "
                echo -e "${COLOR_GREEN}Waiting for RCON connection. Please wait a moment ...${COLOR_NC}"
                while true; do
                    if [ "$(cat /opt/${MCUSER}/logs/latest.log | grep -o "RCON running")" == "RCON running" ]; then
                        ${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} "op ${MC_OWNERIG}"
                        break
                    else
                        sleep 1
                    fi
                done
            else
                while true; do
                    if [ "$(cat /opt/${MCUSER}/logs/latest.log | grep -o "Done")" == "Done" ]; then
                        break
                    else
                        sleep 1
                    fi
                done
            fi

            while true; do
                if [ $(systemctl show -p SubState --value ${MCUSER}) == "running" ]; then
                    break
                fi

                if [ $(systemctl show -p SubState --value ${MCUSER}) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while rebooting the server!"
                    echo
                    journalctl -p err -b -u ${MCUSER}
                    exit
                fi
            done

            reset
            show_logo

            echo
            echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}:${MC_PORT}${COLOR_NC}"
            echo 
            ;;
        --recreate-world|--rcw)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            MCUSER=${dirs[$opt-1]}

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already stopped!"
                                    echo
                                    break
                                fi

                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "running" ]; then                                   
                                    echo " "
                                    echo -e "${COLOR_GREEN}Stopping ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl stop ${dirs[$opt-1]}.service
                                    break
                                fi
                            done

                            sleep 2

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server has been stopped!"
                                    break
                                fi
                            done

                            echo " "
                            echo -e "${COLOR_GREEN}Removing world ...${COLOR_NC}"
                            WORLDNAME=$(config ${dirs[$opt-1]} 'lvlname')
                            rm -r "${SRVPATH}/${WORLDNAME}"
                            rm -r "${SRVPATH}/${WORLDNAME}_nether"
                            rm -r "${SRVPATH}/${WORLDNAME}_the_end"

                            echo " "
                            echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
                            sed -i "s/motd:$(config ${dirs[$opt-1]} 'motd')/motd:$(cat ${SRVPATH}/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/connip:$(config ${dirs[$opt-1]} 'connip')/connip:$(cat ${SRVPATH}/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/max_players:$(config ${dirs[$opt-1]} 'max_players')/max_players:$(cat ${SRVPATH}/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/whitelist:$(config ${dirs[$opt-1]} 'whitelist')/whitelist:$(cat ${SRVPATH}/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/port:$(config ${dirs[$opt-1]} 'port')/port:$(cat ${SRVPATH}/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            if [ "$(cat "${SRVPATH}/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                                sed -i "s/enable-rcon=false/enable-rcon=true/gI" "${SRVPATH}/server.properties"
                            fi

                            rcon_changes_detected=false
                            if [ "$(config ${dirs[$opt-1]} 'rcon_passwd')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi
                            if [ "$(config ${dirs[$opt-1]} 'rcon_port')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi

                            MC_CONNIP=$(config ${dirs[$opt-1]} 'connip')
                            MC_PORT=$(config ${dirs[$opt-1]} 'port')

                            if [ $rcon_changes_detected == true ]; then
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. ${dirs[$opt-1]}.service will be rebuilt!"

                                sed -i "s/rcon_port:$(config ${dirs[$opt-1]} 'rcon_port')/rcon_port:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/rcon_passwd:$(config ${dirs[$opt-1]} 'rcon_passwd')/rcon_passwd:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"

                                echo " "
                                echo -e "${COLOR_GREEN}Stop and disable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl stop ${dirs[$opt-1]}.service
                                systemctl disable ${dirs[$opt-1]}.service
                                rm /lib/systemd/system/${dirs[$opt-1]}.service

                                echo " "
                                echo -e "${COLOR_GREEN}Create ${dirs[$opt-1]}.service ...${COLOR_NC}"

                                MC_RAM=$(config ${dirs[$opt-1]} 'ram')
                                MC_RCON_PORT=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                                MC_RCON_PASSWD=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                                cat << EOF > /lib/systemd/system/${dirs[$opt-1]}.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=${SRVPATH}/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                                echo " "
                                echo -e "${COLOR_GREEN}Enable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl enable ${dirs[$opt-1]}.service
                            fi

                            echo " "
                            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
                            systemctl start ${dirs[$opt-1]}.service
                            sleep 5
                            while true; do
                                if [ "$(cat ${SRVPATH}/logs/latest.log | grep -o "Done")" == "Done" ]; then
                                    reset
                                    show_logo

                                    echo
                                    echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}:${MC_PORT}${COLOR_NC}"
                                    echo
                                    break
                                else
                                    sleep 1
                                fi
                            done

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --upgrade)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            MC_DOWNLOAD=$(config ${dirs[$opt-1]} 'dl')
            MC_VERSION=$2

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            MCUSER=${dirs[$opt-1]}
                
                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    break
                                fi

                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "running" ]; then
                                    echo " "
                                    echo -e "${COLOR_GREEN}Send global message to server ...${COLOR_NC}"
                                    rcon ${dirs[$opt-1]} 'tellraw @a [{"text":"[SERVER] ", "color":"gold"},{"text":"The server will be stopped in 10 seconds!", "color":"red"}]'
                                    sleep 10
                                    rcon ${dirs[$opt-1]} 'save-all'
                                    
                                    echo " "
                                    echo -e "${COLOR_GREEN}Stopping ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl stop ${dirs[$opt-1]}.service

                                    if [ $(config ${dirs[$opt-1]} 'auto_backup') == "true" ]; then
                                        if [ $(config ${dirs[$opt-1]} 'auto_backup_routine' | grep -o -E '(s)') ]; then
                                            echo " "
                                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                                            sleep 3
                                            find ${SRVPATH}/_backups/ -type f -mtime +$(config ${dirs[$opt-1]} 'auto_backup_days') -name '*.gz' -delete
                                            tar --exclude='_tools' --exclude='_backups' -czf ${SRVPATH}/_backups/server_stopped-$(date +%F-%H-%M).tar.gz ${SRVPATH} > /dev/null
                                        fi
                                    fi
                                    break
                                fi
                            done

                            sleep 2

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    break
                                fi
                            done

                            # CHECK MINECRAFT VERSION
                            echo " "
                            echo -e "${COLOR_GREEN}Checking Minecraft version ...${COLOR_NC}"
                            if [ ! $(echo ${MC_VERSION} | grep -o -E '^((([0-9]{1,}).([0-9]{1,}))|(([0-9]{1,}).([0-9]{1,}).([0-9]{1,})))$') ]; then
                                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The version number is invalid!"
                                exit
                            else               
                                if [ $(expr $MC_VERSION | cut -d'.' -f2) -gt 11 ] && [ $(expr $MC_VERSION | cut -d'.' -f2) -lt 20 ]; then
                                    if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                                        shift
                                    elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                                        shift
                                    else
                                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The requested URL does not exist."
                                        echo "         You tried to install \"$MC_DOWNLOAD $MC_VERSION\". Is that correct?"
                                        exit
                                    fi
                                else
                                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Only Minecraft version 1.12 to 1.20.x is supported!"
                                    exit
                                fi
                            fi

                            # CHECK JAVA VERSION
                            check_requirements=true
                            echo " "
                            echo -e "${COLOR_GREEN}Checking if Minecraft version compatible with installed Java version ...${COLOR_NC}"
                            checkJavaVersion ${MC_VERSION}

                            if [ $check_requirements == false ]; then
                                exit
                            fi

                            # REMOVE OLD SERVER FILE
                            echo " "
                            echo -e "${COLOR_GREEN}Remove old server file ...${COLOR_NC}"
                            rm "${SRVPATH}/server.jar"

                            # DOWNLOAD 
                            echo " "
                            echo -e "${COLOR_GREEN}Download $MC_DOWNLOAD $MC_VERSION ...${COLOR_NC}"
                            if wget -q --method=HEAD "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                                wget -q -O "${SRVPATH}/server.jar" "https://download.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
                            elif wget -q --method=HEAD "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"; then
                                wget -q -O "${SRVPATH}/server.jar" "https://cdn.getbukkit.org/$MC_DOWNLOAD/$MC_DOWNLOAD-$MC_VERSION.jar"
                            fi

                            # STARTING PROCESS
                            echo " "
                            echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
                            sed -i "s/version:$(config ${dirs[$opt-1]} 'version')/version:${MC_VERSION}/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/motd:$(config ${dirs[$opt-1]} 'motd')/motd:$(cat ${SRVPATH}/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/connip:$(config ${dirs[$opt-1]} 'connip')/connip:$(cat ${SRVPATH}/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/max_players:$(config ${dirs[$opt-1]} 'max_players')/max_players:$(cat ${SRVPATH}/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/whitelist:$(config ${dirs[$opt-1]} 'whitelist')/whitelist:$(cat ${SRVPATH}/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/port:$(config ${dirs[$opt-1]} 'port')/port:$(cat ${SRVPATH}/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            if [ "$(cat "${SRVPATH}/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                                sed -i "s/enable-rcon=false/enable-rcon=true/gI" "${SRVPATH}/server.properties"
                            fi

                            rcon_changes_detected=false
                            if [ "$(config ${dirs[$opt-1]} 'rcon_passwd')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi
                            if [ "$(config ${dirs[$opt-1]} 'rcon_port')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi

                            MC_CONNIP=$(config ${dirs[$opt-1]} 'connip')
                            MC_PORT=$(config ${dirs[$opt-1]} 'port')

                            if [ $rcon_changes_detected == true ]; then
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. ${dirs[$opt-1]}.service will be rebuilt!"

                                sed -i "s/rcon_port:$(config ${dirs[$opt-1]} 'rcon_port')/rcon_port:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/rcon_passwd:$(config ${dirs[$opt-1]} 'rcon_passwd')/rcon_passwd:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"

                                echo " "
                                echo -e "${COLOR_GREEN}Stop and disable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl stop ${dirs[$opt-1]}.service
                                systemctl disable ${dirs[$opt-1]}.service
                                rm /lib/systemd/system/${dirs[$opt-1]}.service

                                echo " "
                                echo -e "${COLOR_GREEN}Create ${dirs[$opt-1]}.service ...${COLOR_NC}"

                                MC_RAM=$(config ${dirs[$opt-1]} 'ram')
                                MC_RCON_PORT=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                                MC_RCON_PASSWD=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                                cat << EOF > /lib/systemd/system/${dirs[$opt-1]}.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=${SRVPATH}/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                                echo " "
                                echo -e "${COLOR_GREEN}Enable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl enable ${dirs[$opt-1]}.service
                            fi

                            echo " "
                            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
                            systemctl start ${dirs[$opt-1]}.service
                            sleep 2
                            while true; do
                                if [ "$(cat ${SRVPATH}/logs/latest.log | grep -o "Done")" == "Done" ]; then
                                    reset
                                    show_logo

                                    echo
                                    echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}:${MC_PORT}${COLOR_NC}"
                                    echo
                                    break
                                else
                                    sleep 1
                                fi
                            done

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --install-plugin|--iplg)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"

                            echo " "
                            echo -e "${COLOR_GREEN}Download Plugin ...${COLOR_NC}"
                            if wget -q --method=HEAD $2; then
                                wget -q $2 -P "${SRVPATH}/plugins"
                            fi
                            echo " "
                            echo -e "${COLOR_GREEN}The plugin has been downloaded!${COLOR_NC}"
                            echo " "
                            echo -e "${COLOR_GREEN}Reload server ...${COLOR_NC}"
                            rcon ${dirs[$opt-1]} 'save-all'
                            rcon ${dirs[$opt-1]} 'reload'
                            echo " "
                            echo -e "${COLOR_GREEN}The server has been reloaded!${COLOR_NC}"

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --create-backup|--cb)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            BACKUPNAME="server_terminal-$(date +%F-%H-%M).tar.gz"

                            if [ ! -d "${SRVPATH}/_backups" ]; then
                                mkdir "${SRVPATH}/_backups"
                            fi

                            echo " "
                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                            rcon ${dirs[$opt-1]} 'save-all'
                            rcon ${dirs[$opt-1]} 'say §6Create server backup ...'

                            find ${SRVPATH}/_backups/ -type f -mtime +7 -name '*.gz' -delete
                            tar --exclude='_tools' --exclude='_backups' -czf ${SRVPATH}/_backups/${BACKUPNAME} ${SRVPATH}
                            BACKUPSIZE=$(ls -lh ${SRVPATH}/_backups/${BACKUPNAME} | awk '{print  $5}')
                            
                            rcon ${dirs[$opt-1]} 'say §2Server backup complete ...'
                            echo " "
                            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Server backup complete!"
                            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Created backup is ${BACKUPSIZE} in size."
                    esac
                    break
                done
            fi
            ;;
        --remove)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            MCUSER=${dirs[$opt-1]}

                            update_upgrade

                            echo " "
                            echo -e "${COLOR_GREEN}Stop and disable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                            systemctl stop ${dirs[$opt-1]}.service
                            systemctl disable ${dirs[$opt-1]}.service

                            echo " "
                            echo -e "${COLOR_GREEN}Remove backup routine ...${COLOR_NC}"
                            # -> remove cronjob

                            echo " "
                            echo -e "${COLOR_GREEN}Remove ${dirs[$opt-1]} user and files ...${COLOR_NC}"
                            rm /lib/systemd/system/${dirs[$opt-1]}.service
                            pgrep -u $MCUSER | xargs kill -9
                            deluser $MCUSER > /dev/null
                            cd "${SRVPATH}/_tools/mcrcon" && make uninstall > /dev/null && cd /opt
                            rm -r ${SRVPATH}

                            update_upgrade

                            reset
                            show_logo
                            echo
                            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server including user and service has been deleted!"
                            echo

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --start)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                                MCUSER=${dirs[$opt-1]}
                                
                                echo " "
                                echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
                                sed -i "s/motd:$(config ${dirs[$opt-1]} 'motd')/motd:$(cat ${SRVPATH}/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/connip:$(config ${dirs[$opt-1]} 'connip')/connip:$(cat ${SRVPATH}/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/max_players:$(config ${dirs[$opt-1]} 'max_players')/max_players:$(cat ${SRVPATH}/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/whitelist:$(config ${dirs[$opt-1]} 'whitelist')/whitelist:$(cat ${SRVPATH}/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/port:$(config ${dirs[$opt-1]} 'port')/port:$(cat ${SRVPATH}/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                if [ "$(cat "${SRVPATH}/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                                    sed -i "s/enable-rcon=false/enable-rcon=true/gI" "${SRVPATH}/server.properties"
                                fi

                                rcon_changes_detected=false
                                if [ "$(config ${dirs[$opt-1]} 'rcon_passwd')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                                    rcon_changes_detected=true
                                fi
                                if [ "$(config ${dirs[$opt-1]} 'rcon_port')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                                    rcon_changes_detected=true
                                fi

                                MC_CONNIP=$(config ${dirs[$opt-1]} 'connip')

                                if [ $rcon_changes_detected == true ]; then
                                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. ${dirs[$opt-1]}.service will be rebuilt!"

                                    sed -i "s/rcon_port:$(config ${dirs[$opt-1]} 'rcon_port')/rcon_port:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                    sed -i "s/rcon_passwd:$(config ${dirs[$opt-1]} 'rcon_passwd')/rcon_passwd:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"

                                    echo " "
                                    echo -e "${COLOR_GREEN}Stop and disable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl stop ${dirs[$opt-1]}.service
                                    systemctl disable ${dirs[$opt-1]}.service
                                    rm /lib/systemd/system/${dirs[$opt-1]}.service

                                    echo " "
                                    echo -e "${COLOR_GREEN}Create ${dirs[$opt-1]}.service ...${COLOR_NC}"

                                    MC_RAM=$(config ${dirs[$opt-1]} 'ram')
                                    MC_RCON_PORT=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                                    MC_RCON_PASSWD=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                                    cat << EOF > /lib/systemd/system/${dirs[$opt-1]}.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=${SRVPATH}/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                                    echo " "
                                    echo -e "${COLOR_GREEN}Enable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl enable ${dirs[$opt-1]}.service
                                fi

                                echo " "
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
                                systemctl start ${dirs[$opt-1]}.service
                                sleep 2
                                while true; do
                                    if [ "$(cat /opt/${dirs[$opt-1]}/logs/latest.log | grep -o "Done")" == "Done" ]; then
                                        reset
                                        show_logo

                                        echo
                                        echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}:${MC_PORT}${COLOR_NC}"
                                        echo
                                        break
                                    else
                                        sleep 1
                                    fi
                                done
                            else
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already running!"
                            fi

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --status)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo


                            echo "    Servername: $(config ${dirs[$opt-1]} 'srvname')"
                            echo "Server Adresse: $(config ${dirs[$opt-1]} 'connip')"
                            echo "   Server Port: $(config ${dirs[$opt-1]} 'port')"
                            echo "   Server Motd: $(config ${dirs[$opt-1]} 'motd')"
                            echo " "
                            if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "running" ]; then
                                echo -e "  Serverstatus: ${COLOR_GREEN}ONLINE${COLOR_NC}"
                                echo " "
                                MCAPI=$(curl -s "https://api.minetools.eu/ping/$(config ${dirs[$opt-1]} 'connip')/$(config ${dirs[$opt-1]} 'port')")
                                echo " Spieleranzahl: $(echo $MCAPI | jq -r .players.online)/$(echo $MCAPI | jq -r .players.max) (not necessarily current)"
                                echo "       Spieler: ...."
                                echo " "
                                PID=$(systemctl show --property MainPID --value ${dirs[$opt-1]})
                                top -bn1 -p $PID | grep -o ${PID}' .*' > mcsrv_top.temp
                                sed -i 's/ \{1,\}/;/g' mcsrv_top.temp
                                CPU=$(cat mcsrv_top.temp | cut -d';' -f9)
                                RAM=$(cat mcsrv_top.temp | cut -d';' -f10)
                                RES=$(cat mcsrv_top.temp | cut -d';' -f6)

                                echo "           CPU: ${CPU}%"
                                echo "           RAM: ${RAM}% (${RES})"

                                rm mcsrv_top.temp
                            elif [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                echo -e "  Serverstatus: ${COLOR_RED}OFFLINE${COLOR_NC}"
                            fi
                            echo " "

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --live-log)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*) reset; show_logo; journalctl -f -u ${dirs[$opt-1]}; break ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --say)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset;
                            show_logo;

                            while true; do
                                read -p "Say: " say
                                case $say in
                                    (*)
                                        rcon ${dirs[$opt-1]} "say ${say}"
                                        echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Message was sended!"
                                        echo " "
                                esac
                            done
                            
                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --reload)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo
                            
                            echo " "
                            echo -e "${COLOR_GREEN}Reload server ...${COLOR_NC}"
                            rcon ${dirs[$opt-1]} 'save-all'
                            rcon ${dirs[$opt-1]} 'reload'
                            echo " "
                            echo -e "${COLOR_GREEN}The server has been reloaded!${COLOR_NC}"
                            
                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --restart)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            MCUSER=${dirs[$opt-1]}

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already stopped!"
                                    echo
                                    break
                                fi

                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "running" ]; then
                                    echo " "
                                    echo -e "${COLOR_GREEN}Send global message to server ...${COLOR_NC}"
                                    rcon ${dirs[$opt-1]} 'tellraw @a [{"text":"[SERVER] ", "color":"gold"},{"text":"The server will be restarted in 10 seconds!", "color":"red"}]'
                                    sleep 10
                                    rcon ${dirs[$opt-1]} 'save-all'
                                    
                                    echo " "
                                    echo -e "${COLOR_GREEN}Stopping ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl stop ${dirs[$opt-1]}.service

                                    if [ $(config ${dirs[$opt-1]} 'auto_backup') == "true" ]; then
                                        if [ $(config ${dirs[$opt-1]} 'auto_backup_routine' | grep -o -E '(r)') ]; then
                                            echo " "
                                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                                            sleep 3
                                            find ${SRVPATH}/_backups/ -type f -mtime +$(config ${dirs[$opt-1]} 'auto_backup_days') -name '*.gz' -delete
                                            tar --exclude='_tools' --exclude='_backups' -czf ${SRVPATH}/_backups/server_restarted-$(date +%F-%H-%M).tar.gz ${SRVPATH}
                                        fi
                                    fi
                                    break
                                fi
                            done

                            sleep 2

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server has been stopped!"
                                    break
                                fi
                            done

                            echo " "
                            echo -e "${COLOR_GREEN}Compare and update the CMCSRV config and server.properties ...${COLOR_NC}"
                            sed -i "s/motd:$(config ${dirs[$opt-1]} 'motd')/motd:$(cat ${SRVPATH}/server.properties | grep -o 'motd=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/connip:$(config ${dirs[$opt-1]} 'connip')/connip:$(cat ${SRVPATH}/server.properties | grep -o 'server-ip=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/max_players:$(config ${dirs[$opt-1]} 'max_players')/max_players:$(cat ${SRVPATH}/server.properties | grep -o 'max-players=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/whitelist:$(config ${dirs[$opt-1]} 'whitelist')/whitelist:$(cat ${SRVPATH}/server.properties | grep -o 'white-list=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            sed -i "s/port:$(config ${dirs[$opt-1]} 'port')/port:$(cat ${SRVPATH}/server.properties | grep -o 'server-port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                            if [ "$(cat "${SRVPATH}/server.properties" | grep -o 'enable-rcon=.*' | cut -d'=' -f2)" == "false" ]; then
                                sed -i "s/enable-rcon=false/enable-rcon=true/gI" "${SRVPATH}/server.properties"
                            fi

                            rcon_changes_detected=false
                            if [ "$(config ${dirs[$opt-1]} 'rcon_passwd')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi
                            if [ "$(config ${dirs[$opt-1]} 'rcon_port')" != "$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)" ]; then
                                rcon_changes_detected=true
                            fi

                            MC_CONNIP=$(config ${dirs[$opt-1]} 'connip')
                            MC_PORT=$(config ${dirs[$opt-1]} 'port')

                            if [ $rcon_changes_detected == true ]; then
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Changes to RCON entries detected. ${dirs[$opt-1]}.service will be rebuilt!"

                                sed -i "s/rcon_port:$(config ${dirs[$opt-1]} 'rcon_port')/rcon_port:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.port=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"
                                sed -i "s/rcon_passwd:$(config ${dirs[$opt-1]} 'rcon_passwd')/rcon_passwd:$(cat ${SRVPATH}/server.properties | grep -o 'rcon.password=.*' | cut -d'=' -f2)/gI" "${SRVPATH}/_cfg.cmcsrv"

                                echo " "
                                echo -e "${COLOR_GREEN}Stop and disable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl stop ${dirs[$opt-1]}.service
                                systemctl disable ${dirs[$opt-1]}.service
                                rm /lib/systemd/system/${dirs[$opt-1]}.service

                                echo " "
                                echo -e "${COLOR_GREEN}Create ${dirs[$opt-1]}.service ...${COLOR_NC}"

                                MC_RAM=$(config ${dirs[$opt-1]} 'ram')
                                MC_RCON_PORT=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.port=.*' | cut -d'=' -f2)
                                MC_RCON_PASSWD=$(cat "${SRVPATH}/server.properties" | grep -o 'rcon.password=.*' | cut -d'=' -f2)

                                cat << EOF > /lib/systemd/system/${dirs[$opt-1]}.service
[Unit]
Description=Minecraft Server
After=network.target
[Service]
WorkingDirectory=${SRVPATH}/
User=$MCUSER
Group=$MCUSER
Nice=1
KillMode=none
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
ExecStart=/usr/bin/java -Xms${MC_RAM}M -Xmx${MC_RAM}M -jar server.jar nogui
ExecStop=${SRVPATH}/_tools/mcrcon/mcrcon -H ${MC_CONNIP} -P ${MC_RCON_PORT} -p ${MC_RCON_PASSWD} stop
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                                echo " "
                                echo -e "${COLOR_GREEN}Enable ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                systemctl enable ${dirs[$opt-1]}.service
                            fi

                            echo " "
                            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is starting! Please wait a moment ..."
                            systemctl start ${dirs[$opt-1]}.service
                            sleep 5
                            while true; do
                                if [ "$(cat ${SRVPATH}/logs/latest.log | grep -o "Done")" == "Done" ]; then
                                    reset
                                    show_logo

                                    echo
                                    echo -e "Your Minecraft server is now reachable on ${COLOR_YELLOW}${MC_CONNIP}:${MC_PORT}${COLOR_NC}"
                                    echo
                                    break
                                else
                                    sleep 1
                                fi
                            done

                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        --stop)
            reset
            show_logo

            dirs=($(find /opt/ -name "minecraft*" | cut -d'/' -f3 | sort))

            if [ ${#dirs[@]} -gt 0 ]; then
                list_servers

                echo " "
                while true; do
                    read -p "Select your Minecraft server: " opt
                    case $opt in
                        (*[0-9]*)
                            reset
                            show_logo

                            SRVPATH="${INSTALLPATH}/${dirs[$opt-1]}"
                            
                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Minecraft server is already stopped!"
                                    echo
                                    exit
                                fi

                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "running" ]; then
                                    echo " "
                                    echo -e "${COLOR_GREEN}Send global message to server ...${COLOR_NC}"
                                    rcon ${dirs[$opt-1]} 'tellraw @a [{"text":"[SERVER] ", "color":"gold"},{"text":"The server will be stopped in 10 seconds!", "color":"red"}]'
                                    sleep 10
                                    rcon ${dirs[$opt-1]} 'save-all'
                                    
                                    echo " "
                                    echo -e "${COLOR_GREEN}Stopping ${dirs[$opt-1]}.service ...${COLOR_NC}"
                                    systemctl stop ${dirs[$opt-1]}.service

                                    if [ $(config ${dirs[$opt-1]} 'auto_backup') == "true" ]; then
                                        if [ $(config ${dirs[$opt-1]} 'auto_backup_routine' | grep -o -E '(s)') ]; then
                                            echo " "
                                            echo -e "${COLOR_GREEN}Create backup ...${COLOR_NC}"
                                            sleep 3
                                            find ${SRVPATH}/_backups/ -type f -mtime +$(config ${dirs[$opt-1]} 'auto_backup_days') -name '*.gz' -delete
                                            tar --exclude='_tools' --exclude='_backups' -czf ${SRVPATH}/_backups/server_stopped-$(date +%F-%H-%M).tar.gz ${SRVPATH} > /dev/null
                                        fi
                                    fi
                                    break
                                fi
                            done

                            sleep 2

                            while true; do
                                if [ $(systemctl show -p SubState --value ${dirs[$opt-1]}) == "dead" ]; then
                                    echo " "
                                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Minecraft server has been stopped!"
                                    echo
                                    exit
                                fi
                            done
                            
                            break
                            ;;
                    esac
                done
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: You dont have an installed Minecraft server!"
                echo
                exit
            fi
            ;;
        -?*)
            reset
            show_logo
            echo " "
            echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: There was a problem with the \"$1 $2 $3\" command!"
            echo "         Use \"-h\" for help."
            echo " "
            exit
            ;;
        *)
            break
    esac

    shift
done